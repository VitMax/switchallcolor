﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Threading;

namespace WpfApplicationLAB_PrSlSis
{
    public static class ExtensionMethods
    {
        private static Action EmptyDelegate = delegate() { };
        public static void Refresh(this UIElement uielement)
        {
            uielement.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
        }
    }

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    ///

    public partial class MainWindow : Window
    {
        private delegate void paintDelegate();
        Color color = new Color();
        List<byte> forvardValues = new List<byte>();
        List<byte> reverseValues = new List<byte>();
        
        public MainWindow()
        {
            InitializeComponent();
            for (int i = 0; i <= 255; i++)
            {
                forvardValues.Add((byte)i);
                reverseValues.Add((byte)i);
            }            
            reverseValues.Reverse();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(new paintDelegate(Paint), null);                    
        }
                
        private void Paint()
        {           
            color.A = 255;
            makeOneColorTransition(forvardValues, "G");                  
            makeOneColorTransition(forvardValues, "B");
            makeOneColorTransition(reverseValues, "G");
            makeOneColorTransition(forvardValues, "R");
            makeOneColorTransition(reverseValues, "B");
            makeOneColorTransition(forvardValues, "G");
            makeOneColorTransition(forvardValues, "B");
            makeOneColorTransition(reverseValues, "A");
        }

        private void makeOneColorTransition(List<byte> values, string baseColor)
        {
            
            values.ForEach(value =>
            {
                switch (baseColor)
                {
                    case "R": color.R = value; break;
                    case "G": color.G = value; break;
                    case "B": color.B = value; break;
                    case "A": color.R = value;
                              color.G = value;
                              color.B = value; break;
                }
                grid.Background = new SolidColorBrush(color);
                grid.Refresh();
                Thread.Sleep(15);
            });
        }
    }
}
